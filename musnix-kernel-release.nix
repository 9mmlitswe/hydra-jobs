let
  pkgs = import <nixpkgs> {};

  jobs = rec {
    hello = pkgs.releaseTools.nixBuild {
      name = "hello";
      src = pkgs.fetchurl {
        url = "https://ftp.gnu.org/gnu/hello/hello-2.10.tar.gz";
        sha256 = "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i";
      };
      configureFlags = [ "--disable-silent-rules " ];
    };

    silicate_system = (import <nixpkgs/nixos> { configuration = ./silicate-kernel.nix; }).system;

    altocumulus_system = (import <nixpkgs/nixos> { configuration = ./altocumulus-kernel.nix; }).system;

  };
in
  jobs

