let
  pkgs = import <nixpkgs> {};

  jobs = rec {

    crystal = pkgs.crystal;
    shards = pkgs.shards;
    pcmanx-gtk2 = pkgs.pcmanx-gtk2;
    opencc = pkgs.opencc;
    fcitx-rime = pkgs.fcitx-engines.rime;
    fcitx-mozc = pkgs.fcitx-engines.mozc;
    pgdl = pkgs.haskellPackages.pgdl;
    rstudio = pkgs.rstudio;
    wineStaging = pkgs.wineStaging;
    darkhttpd = pkgs.darkhttpd;
    tig = pkgs.tig;
    terminator = pkgs.terminator;
    tdesktop = pkgs.tdesktop;
    spin = pkgs.spin;

    python2-powerline = pkgs.python27Packages.powerline;
    python3-powerline = pkgs.python36Packages.powerline;

  };
in
  jobs

