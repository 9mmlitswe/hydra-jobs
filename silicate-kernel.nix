{ config, pkgs, ... }:

{
  imports = [
    <musnix>
    ./hardware-configuration.nix
  ];

  boot.loader = {
    efi.canTouchEfiVariables = true;
    timeout = 2;
    # Use the GRUB 2 boot loader.
    grub = {
      enable = true;
      efiSupport = true;
      device = "nodev";
    };
  };

  security.rtkit.enable = true;

  musnix = {
    enable = true;
    kernel = {
      optimize = true;
      realtime = true;
      packages = pkgs.linuxPackages_latest_rt;
    };
    rtirq.enable = true;
  };

  system.stateVersion = "17.03";
}

