{ config, pkgs, ... }:

{
  imports = [
    <musnix>
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;

  musnix = {
    enable = true;
    kernel = {
      optimize = true;
      realtime = true;
    };
    rtirq.enable = true;
  };

}

